# BuildStream Test Base

An custom image containing plugin dependencies to be used in bst-plugins-\*
test suites.

## Rationale

As plugins for different buildsystems and outputs appear, it starts to increase
test times with all of the new dependencies. We want our test suite to be as 
lightweight as possible, and so the idea is to build a small image with all the
dependencies we need.
